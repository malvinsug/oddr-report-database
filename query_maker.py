reducer_list = ["None", "PCA", "UMAP", "tSNE"]
detector_list = ["ABOD", "COF", "COPOD", "IForest", "iNNE", "KNN", "LOF"]
score_list = ["accuracy","precision","recall","matthew_corrcoef","cohen_kappa"]
a_dic = {
    "NoneABOD":"na",
    "NoneCOF":"nc",
    "NoneCOPOD":"ncp",
    "NoneIForest":"nif",
    "NoneiNNE":"nin",
    "NoneKNN":"nk",
    "NoneLOF":"nl",
    "PCAABOD": "pa",
    "PCACOF": "pc",
    "PCACOPOD": "pcp",
    "PCAIForest": "pif",
    "PCAiNNE": "pin",
    "PCAKNN": "pk",
    "PCALOF": "pl",
    "UMAPABOD": "ua",
    "UMAPCOF": "uc",
    "UMAPCOPOD": "ucp",
    "UMAPIForest": "uif",
    "UMAPiNNE": "uin",
    "UMAPKNN": "uk",
    "UMAPLOF": "ul",
    "tSNEABOD": "ta",
    "tSNECOF": "tc",
    "tSNECOPOD": "tcp",
    "tSNEIForest": "tif",
    "tSNEiNNE": "tin",
    "tSNEKNN": "tk",
    "tSNELOF": "tl",
}

s_dic = {
    "accuracy":"accuracy_score",
    "precision":"precision_score",
    "recall":"recall_score",
    "matthew_corrcoef":"matthew_corrcoef_score",
    "cohen_kappa":"cohen_kappa_score"
}


if __name__ == "__main__":
    for detector_name in detector_list:
        for score_name in score_list:
            QUERY_MASTER = f'CREATE VIEW {detector_name}_{s_dic[score_name]} AS \n' \
                           f'SELECT {a_dic[reducer_list[0]+detector_name]}.dataset_name,{a_dic[reducer_list[0]+detector_name]}.{s_dic[score_name]} AS "{reducer_list[0]}_{score_name}",{a_dic[reducer_list[1]+detector_name]}.{s_dic[score_name]} AS "{reducer_list[1]}_{score_name}", {a_dic[reducer_list[2]+detector_name]}.{s_dic[score_name]} AS "{reducer_list[2]}_{score_name}"  \n' \
                           f'FROM {reducer_list[0]}_{detector_name} {a_dic[reducer_list[0]+detector_name]}  INNER JOIN {reducer_list[1]}_{detector_name} {a_dic[reducer_list[1]+detector_name]}  ON {a_dic[reducer_list[0]+detector_name]}.dataset_name = {a_dic[reducer_list[1]+detector_name]}.dataset_name  \n' \
                           f'INNER JOIN {reducer_list[2]}_{detector_name} {a_dic[reducer_list[2]+detector_name]}  ON {a_dic[reducer_list[2]+detector_name]}.dataset_name = {a_dic[reducer_list[1]+detector_name]}.dataset_name AND {a_dic[reducer_list[2]+detector_name]}.dataset_name = {a_dic[reducer_list[0]+detector_name]}.dataset_name'

            print(QUERY_MASTER)
            print("")

