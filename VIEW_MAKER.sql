CREATE VIEW ABOD_accuracy_score AS
SELECT na.dataset_name,na.accuracy_score AS "None_accuracy",pa.accuracy_score AS "PCA_accuracy", ua.accuracy_score AS "UMAP_accuracy"
FROM None_ABOD na  INNER JOIN PCA_ABOD pa  ON na.dataset_name = pa.dataset_name
INNER JOIN UMAP_ABOD ua  ON ua.dataset_name = pa.dataset_name AND ua.dataset_name = na.dataset_name

CREATE VIEW ABOD_precision_score AS
SELECT na.dataset_name,na.precision_score AS "None_precision",pa.precision_score AS "PCA_precision", ua.precision_score AS "UMAP_precision"
FROM None_ABOD na  INNER JOIN PCA_ABOD pa  ON na.dataset_name = pa.dataset_name
INNER JOIN UMAP_ABOD ua  ON ua.dataset_name = pa.dataset_name AND ua.dataset_name = na.dataset_name

CREATE VIEW ABOD_recall_score AS
SELECT na.dataset_name,na.recall_score AS "None_recall",pa.recall_score AS "PCA_recall", ua.recall_score AS "UMAP_recall"
FROM None_ABOD na  INNER JOIN PCA_ABOD pa  ON na.dataset_name = pa.dataset_name
INNER JOIN UMAP_ABOD ua  ON ua.dataset_name = pa.dataset_name AND ua.dataset_name = na.dataset_name

CREATE VIEW ABOD_matthew_corrcoef AS
SELECT na.dataset_name,na.matthew_corrcoef AS "None_matthew_corrcoef",pa.matthew_corrcoef AS "PCA_matthew_corrcoef", ua.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_ABOD na  INNER JOIN PCA_ABOD pa  ON na.dataset_name = pa.dataset_name
INNER JOIN UMAP_ABOD ua  ON ua.dataset_name = pa.dataset_name AND ua.dataset_name = na.dataset_name

CREATE VIEW ABOD_cohen_kappa_score AS
SELECT na.dataset_name,na.cohen_kappa_score AS "None_cohen_kappa",pa.cohen_kappa_score AS "PCA_cohen_kappa", ua.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_ABOD na  INNER JOIN PCA_ABOD pa  ON na.dataset_name = pa.dataset_name
INNER JOIN UMAP_ABOD ua  ON ua.dataset_name = pa.dataset_name AND ua.dataset_name = na.dataset_name

CREATE VIEW COF_accuracy_score AS
SELECT nc.dataset_name,nc.accuracy_score AS "None_accuracy",pc.accuracy_score AS "PCA_accuracy", uc.accuracy_score AS "UMAP_accuracy"
FROM None_COF nc  INNER JOIN PCA_COF pc  ON nc.dataset_name = pc.dataset_name
INNER JOIN UMAP_COF uc  ON uc.dataset_name = pc.dataset_name AND uc.dataset_name = nc.dataset_name

CREATE VIEW COF_precision_score AS
SELECT nc.dataset_name,nc.precision_score AS "None_precision",pc.precision_score AS "PCA_precision", uc.precision_score AS "UMAP_precision"
FROM None_COF nc  INNER JOIN PCA_COF pc  ON nc.dataset_name = pc.dataset_name
INNER JOIN UMAP_COF uc  ON uc.dataset_name = pc.dataset_name AND uc.dataset_name = nc.dataset_name

CREATE VIEW COF_recall_score AS
SELECT nc.dataset_name,nc.recall_score AS "None_recall",pc.recall_score AS "PCA_recall", uc.recall_score AS "UMAP_recall"
FROM None_COF nc  INNER JOIN PCA_COF pc  ON nc.dataset_name = pc.dataset_name
INNER JOIN UMAP_COF uc  ON uc.dataset_name = pc.dataset_name AND uc.dataset_name = nc.dataset_name

CREATE VIEW COF_matthew_corrcoef AS
SELECT nc.dataset_name,nc.matthew_corrcoef AS "None_matthew_corrcoef",pc.matthew_corrcoef AS "PCA_matthew_corrcoef", uc.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_COF nc  INNER JOIN PCA_COF pc  ON nc.dataset_name = pc.dataset_name
INNER JOIN UMAP_COF uc  ON uc.dataset_name = pc.dataset_name AND uc.dataset_name = nc.dataset_name

CREATE VIEW COF_cohen_kappa_score AS
SELECT nc.dataset_name,nc.cohen_kappa_score AS "None_cohen_kappa",pc.cohen_kappa_score AS "PCA_cohen_kappa", uc.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_COF nc  INNER JOIN PCA_COF pc  ON nc.dataset_name = pc.dataset_name
INNER JOIN UMAP_COF uc  ON uc.dataset_name = pc.dataset_name AND uc.dataset_name = nc.dataset_name

CREATE VIEW COPOD_accuracy_score AS
SELECT ncp.dataset_name,ncp.accuracy_score AS "None_accuracy",pcp.accuracy_score AS "PCA_accuracy", ucp.accuracy_score AS "UMAP_accuracy"
FROM None_COPOD ncp  INNER JOIN PCA_COPOD pcp  ON ncp.dataset_name = pcp.dataset_name
INNER JOIN UMAP_COPOD ucp  ON ucp.dataset_name = pcp.dataset_name AND ucp.dataset_name = ncp.dataset_name

CREATE VIEW COPOD_precision_score AS
SELECT ncp.dataset_name,ncp.precision_score AS "None_precision",pcp.precision_score AS "PCA_precision", ucp.precision_score AS "UMAP_precision"
FROM None_COPOD ncp  INNER JOIN PCA_COPOD pcp  ON ncp.dataset_name = pcp.dataset_name
INNER JOIN UMAP_COPOD ucp  ON ucp.dataset_name = pcp.dataset_name AND ucp.dataset_name = ncp.dataset_name

CREATE VIEW COPOD_recall_score AS
SELECT ncp.dataset_name,ncp.recall_score AS "None_recall",pcp.recall_score AS "PCA_recall", ucp.recall_score AS "UMAP_recall"
FROM None_COPOD ncp  INNER JOIN PCA_COPOD pcp  ON ncp.dataset_name = pcp.dataset_name
INNER JOIN UMAP_COPOD ucp  ON ucp.dataset_name = pcp.dataset_name AND ucp.dataset_name = ncp.dataset_name

CREATE VIEW COPOD_matthew_corrcoef AS
SELECT ncp.dataset_name,ncp.matthew_corrcoef AS "None_matthew_corrcoef",pcp.matthew_corrcoef AS "PCA_matthew_corrcoef", ucp.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_COPOD ncp  INNER JOIN PCA_COPOD pcp  ON ncp.dataset_name = pcp.dataset_name
INNER JOIN UMAP_COPOD ucp  ON ucp.dataset_name = pcp.dataset_name AND ucp.dataset_name = ncp.dataset_name

CREATE VIEW COPOD_cohen_kappa_score AS
SELECT ncp.dataset_name,ncp.cohen_kappa_score AS "None_cohen_kappa",pcp.cohen_kappa_score AS "PCA_cohen_kappa", ucp.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_COPOD ncp  INNER JOIN PCA_COPOD pcp  ON ncp.dataset_name = pcp.dataset_name
INNER JOIN UMAP_COPOD ucp  ON ucp.dataset_name = pcp.dataset_name AND ucp.dataset_name = ncp.dataset_name

CREATE VIEW IForest_accuracy_score AS
SELECT nif.dataset_name,nif.accuracy_score AS "None_accuracy",pif.accuracy_score AS "PCA_accuracy", uif.accuracy_score AS "UMAP_accuracy"
FROM None_IForest nif  INNER JOIN PCA_IForest pif  ON nif.dataset_name = pif.dataset_name
INNER JOIN UMAP_IForest uif  ON uif.dataset_name = pif.dataset_name AND uif.dataset_name = nif.dataset_name

CREATE VIEW IForest_precision_score AS
SELECT nif.dataset_name,nif.precision_score AS "None_precision",pif.precision_score AS "PCA_precision", uif.precision_score AS "UMAP_precision"
FROM None_IForest nif  INNER JOIN PCA_IForest pif  ON nif.dataset_name = pif.dataset_name
INNER JOIN UMAP_IForest uif  ON uif.dataset_name = pif.dataset_name AND uif.dataset_name = nif.dataset_name

CREATE VIEW IForest_recall_score AS
SELECT nif.dataset_name,nif.recall_score AS "None_recall",pif.recall_score AS "PCA_recall", uif.recall_score AS "UMAP_recall"
FROM None_IForest nif  INNER JOIN PCA_IForest pif  ON nif.dataset_name = pif.dataset_name
INNER JOIN UMAP_IForest uif  ON uif.dataset_name = pif.dataset_name AND uif.dataset_name = nif.dataset_name

CREATE VIEW IForest_matthew_corrcoef AS
SELECT nif.dataset_name,nif.matthew_corrcoef AS "None_matthew_corrcoef",pif.matthew_corrcoef AS "PCA_matthew_corrcoef", uif.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_IForest nif  INNER JOIN PCA_IForest pif  ON nif.dataset_name = pif.dataset_name
INNER JOIN UMAP_IForest uif  ON uif.dataset_name = pif.dataset_name AND uif.dataset_name = nif.dataset_name

CREATE VIEW IForest_cohen_kappa_score AS
SELECT nif.dataset_name,nif.cohen_kappa_score AS "None_cohen_kappa",pif.cohen_kappa_score AS "PCA_cohen_kappa", uif.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_IForest nif  INNER JOIN PCA_IForest pif  ON nif.dataset_name = pif.dataset_name
INNER JOIN UMAP_IForest uif  ON uif.dataset_name = pif.dataset_name AND uif.dataset_name = nif.dataset_name

CREATE VIEW iNNE_accuracy_score AS
SELECT nin.dataset_name,nin.accuracy_score AS "None_accuracy",pin.accuracy_score AS "PCA_accuracy", uin.accuracy_score AS "UMAP_accuracy"
FROM None_iNNE nin  INNER JOIN PCA_iNNE pin  ON nin.dataset_name = pin.dataset_name
INNER JOIN UMAP_iNNE uin  ON uin.dataset_name = pin.dataset_name AND uin.dataset_name = nin.dataset_name

CREATE VIEW iNNE_precision_score AS
SELECT nin.dataset_name,nin.precision_score AS "None_precision",pin.precision_score AS "PCA_precision", uin.precision_score AS "UMAP_precision"
FROM None_iNNE nin  INNER JOIN PCA_iNNE pin  ON nin.dataset_name = pin.dataset_name
INNER JOIN UMAP_iNNE uin  ON uin.dataset_name = pin.dataset_name AND uin.dataset_name = nin.dataset_name

CREATE VIEW iNNE_recall_score AS
SELECT nin.dataset_name,nin.recall_score AS "None_recall",pin.recall_score AS "PCA_recall", uin.recall_score AS "UMAP_recall"
FROM None_iNNE nin  INNER JOIN PCA_iNNE pin  ON nin.dataset_name = pin.dataset_name
INNER JOIN UMAP_iNNE uin  ON uin.dataset_name = pin.dataset_name AND uin.dataset_name = nin.dataset_name

CREATE VIEW iNNE_matthew_corrcoef AS
SELECT nin.dataset_name,nin.matthew_corrcoef AS "None_matthew_corrcoef",pin.matthew_corrcoef AS "PCA_matthew_corrcoef", uin.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_iNNE nin  INNER JOIN PCA_iNNE pin  ON nin.dataset_name = pin.dataset_name
INNER JOIN UMAP_iNNE uin  ON uin.dataset_name = pin.dataset_name AND uin.dataset_name = nin.dataset_name

CREATE VIEW iNNE_cohen_kappa_score AS
SELECT nin.dataset_name,nin.cohen_kappa_score AS "None_cohen_kappa",pin.cohen_kappa_score AS "PCA_cohen_kappa", uin.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_iNNE nin  INNER JOIN PCA_iNNE pin  ON nin.dataset_name = pin.dataset_name
INNER JOIN UMAP_iNNE uin  ON uin.dataset_name = pin.dataset_name AND uin.dataset_name = nin.dataset_name

CREATE VIEW KNN_accuracy_score AS
SELECT nk.dataset_name,nk.accuracy_score AS "None_accuracy",pk.accuracy_score AS "PCA_accuracy", uk.accuracy_score AS "UMAP_accuracy"
FROM None_KNN nk  INNER JOIN PCA_KNN pk  ON nk.dataset_name = pk.dataset_name
INNER JOIN UMAP_KNN uk  ON uk.dataset_name = pk.dataset_name AND uk.dataset_name = nk.dataset_name

CREATE VIEW KNN_precision_score AS
SELECT nk.dataset_name,nk.precision_score AS "None_precision",pk.precision_score AS "PCA_precision", uk.precision_score AS "UMAP_precision"
FROM None_KNN nk  INNER JOIN PCA_KNN pk  ON nk.dataset_name = pk.dataset_name
INNER JOIN UMAP_KNN uk  ON uk.dataset_name = pk.dataset_name AND uk.dataset_name = nk.dataset_name

CREATE VIEW KNN_recall_score AS
SELECT nk.dataset_name,nk.recall_score AS "None_recall",pk.recall_score AS "PCA_recall", uk.recall_score AS "UMAP_recall"
FROM None_KNN nk  INNER JOIN PCA_KNN pk  ON nk.dataset_name = pk.dataset_name
INNER JOIN UMAP_KNN uk  ON uk.dataset_name = pk.dataset_name AND uk.dataset_name = nk.dataset_name

CREATE VIEW KNN_matthew_corrcoef AS
SELECT nk.dataset_name,nk.matthew_corrcoef AS "None_matthew_corrcoef",pk.matthew_corrcoef AS "PCA_matthew_corrcoef", uk.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_KNN nk  INNER JOIN PCA_KNN pk  ON nk.dataset_name = pk.dataset_name
INNER JOIN UMAP_KNN uk  ON uk.dataset_name = pk.dataset_name AND uk.dataset_name = nk.dataset_name

CREATE VIEW KNN_cohen_kappa_score AS
SELECT nk.dataset_name,nk.cohen_kappa_score AS "None_cohen_kappa",pk.cohen_kappa_score AS "PCA_cohen_kappa", uk.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_KNN nk  INNER JOIN PCA_KNN pk  ON nk.dataset_name = pk.dataset_name
INNER JOIN UMAP_KNN uk  ON uk.dataset_name = pk.dataset_name AND uk.dataset_name = nk.dataset_name

CREATE VIEW LOF_accuracy_score AS
SELECT nl.dataset_name,nl.accuracy_score AS "None_accuracy",pl.accuracy_score AS "PCA_accuracy", ul.accuracy_score AS "UMAP_accuracy"
FROM None_LOF nl  INNER JOIN PCA_LOF pl  ON nl.dataset_name = pl.dataset_name
INNER JOIN UMAP_LOF ul  ON ul.dataset_name = pl.dataset_name AND ul.dataset_name = nl.dataset_name

CREATE VIEW LOF_precision_score AS
SELECT nl.dataset_name,nl.precision_score AS "None_precision",pl.precision_score AS "PCA_precision", ul.precision_score AS "UMAP_precision"
FROM None_LOF nl  INNER JOIN PCA_LOF pl  ON nl.dataset_name = pl.dataset_name
INNER JOIN UMAP_LOF ul  ON ul.dataset_name = pl.dataset_name AND ul.dataset_name = nl.dataset_name

CREATE VIEW LOF_recall_score AS
SELECT nl.dataset_name,nl.recall_score AS "None_recall",pl.recall_score AS "PCA_recall", ul.recall_score AS "UMAP_recall"
FROM None_LOF nl  INNER JOIN PCA_LOF pl  ON nl.dataset_name = pl.dataset_name
INNER JOIN UMAP_LOF ul  ON ul.dataset_name = pl.dataset_name AND ul.dataset_name = nl.dataset_name

CREATE VIEW LOF_matthew_corrcoef AS
SELECT nl.dataset_name,nl.matthew_corrcoef AS "None_matthew_corrcoef",pl.matthew_corrcoef AS "PCA_matthew_corrcoef", ul.matthew_corrcoef AS "UMAP_matthew_corrcoef"
FROM None_LOF nl  INNER JOIN PCA_LOF pl  ON nl.dataset_name = pl.dataset_name
INNER JOIN UMAP_LOF ul  ON ul.dataset_name = pl.dataset_name AND ul.dataset_name = nl.dataset_name

CREATE VIEW LOF_cohen_kappa_score AS
SELECT nl.dataset_name,nl.cohen_kappa_score AS "None_cohen_kappa",pl.cohen_kappa_score AS "PCA_cohen_kappa", ul.cohen_kappa_score AS "UMAP_cohen_kappa"
FROM None_LOF nl  INNER JOIN PCA_LOF pl  ON nl.dataset_name = pl.dataset_name
INNER JOIN UMAP_LOF ul  ON ul.dataset_name = pl.dataset_name AND ul.dataset_name = nl.dataset_name
